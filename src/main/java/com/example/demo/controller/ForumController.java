package com.example.demo.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@RequestParam(name="startDate", required = false) String startDate, @RequestParam(name="endDate", required = false) String endDate) {
		ModelAndView mav = new ModelAndView();

		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport(startDate, endDate);
		Collections.sort(contentData, Comparator.comparing(Report::getUpdatedDate).reversed());

		// 画面遷移先を指定
		mav.setViewName("/top");

		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);

		// コメントを全件取得
		List<Comment> commentData = commentService.findAllComment();

		// コメントデータオブジェクトを保管
		mav.addObject("comments", commentData);

		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();

		// form用の空のentityを準備
		Report report = new Report();

		// 画面遷移先を指定
		mav.setViewName("/new");

		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {

		// 日付を設定
		report.setCreatedDate(new Date());
		report.setUpdatedDate(new Date());

		// 投稿をテーブルに格納
		reportService.saveReport(report);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {

		// UrlParameterのidを基に投稿を削除
		reportService.deleteReport(id);

		// rootヘリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();

		// 編集する投稿を取得
		Report report = reportService.editReport(id);

		// 編集する投稿をセット
		mav.addObject("formModel", report);

		// 画面遷移先を指定
		mav.setViewName("/edit");

		return mav;
	}


	// 投稿編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel") Report report) {

		// UrlParameterのidを更新するentityにセット
		report.setId(id);

		report.setUpdatedDate(new Date());

		// 編集した投稿を更新
		reportService.saveReport(report);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント処理
	@PostMapping("/comment")
	public ModelAndView newComment(@ModelAttribute("formModel") Comment comment) {

		// 作成日時・更新日時を設定
		comment.setCreatedDate(new Date());
		comment.setUpdatedDate(new Date());

		// コメントをテーブルに格納
		commentService.saveComment(comment);

		// reportの更新日時を設定
		Report report = reportService.editReport(comment.getReportId());
		report.setUpdatedDate(comment.getUpdatedDate());
		reportService.saveReport(report);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント削除処理
		@DeleteMapping("/deleteComment/{id}")
		public ModelAndView deleteComment(@PathVariable Integer id) {

			// UrlParameterのidを基に投稿を削除
			commentService.deleteComment(id);

			// rootヘリダイレクト
			return new ModelAndView("redirect:/");
		}

	// コメント編集画面
		@GetMapping("/editComment/{id}")
		public ModelAndView editComment(@PathVariable Integer id) {
			ModelAndView mav = new ModelAndView();

			// 編集するコメントを取得
			Comment comment = commentService.editComment(id);

			// 編集するコメントをセット
			mav.addObject("formModel", comment);

			// 画面遷移先を指定
			mav.setViewName("/editComment");

			return mav;
		}


		// コメント編集処理
		@PutMapping("/updateComment/{id}")
		public ModelAndView updateComment (@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {

			// UrlParameterのidを更新するentityにセット
			comment.setId(id);

			comment.setUpdatedDate(new Date());

			// 編集したコメントを更新
			commentService.saveComment(comment);

			// rootへリダイレクト
			return new ModelAndView("redirect:/");
		}
}
