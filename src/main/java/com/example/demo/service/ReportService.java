package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport(String startDate, String endDate) {

		if(startDate == null || startDate.isEmpty()) {
			startDate = "2020/01/01 00:00:00";
		} else {
			startDate = startDate.replace("-", "/");
			startDate += " 00:00:00";
		}
		if(endDate == null || endDate.isEmpty()) {
			Date currentDate = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String defaultDay = format.format(currentDate);
			endDate = defaultDay;
		} else {
			endDate = endDate.replace("-", "/");
			endDate += " 23:59:59";
		}

		Date start = null;
		Date end = null;

		// String型からDate型への変換処理
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			start = format.parse(startDate);
			end = format.parse(endDate);

		} catch(ParseException e) {
			e.printStackTrace();
		}

		return reportRepository.findByCreatedDateBetween(start, end);
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード1件削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// レコード1件取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

}
