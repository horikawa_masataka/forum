package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reports")

public class Report {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String content;

	@Column(name = "createdDate", insertable = true, updatable = false)
	private Date createdDate;

	@Column
	private Date updatedDate;

	// 投稿IDを取得
	public int getId() {
		return id;
	}
	// 投稿IDを格納
	public void setId(int id) {
		this.id = id;
	}

	// 投稿内容を取得
	public String getContent() {
		return content;
	}
	// 投稿内容を格納
	public void setContent(String content) {
		this.content = content;
	}

	// 作成日時を取得
	public Date getCreatedDate() {
		return createdDate;
	}
	// 作成日時を格納
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	// 更新日時を取得
	public Date getUpdatedDate() {
		return updatedDate;
	}
	// 更新日時を格納
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
